# -*- coding: utf-8 -*-
# 2023/10/11 18:11
# @Author: hjf
# @File: redistest


import redis

try:
    r = redis.Redis(host='127.0.0.1',port=6379,db=0)
    r.ping()
    r.set('123', '234')
    print("Redis连接成功")
except redis.ConnectionError:
    print("无法连接到Redis")