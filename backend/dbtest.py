# -*- coding: utf-8 -*-
# 2023/10/11 17:20
# @Author: hjf
# @File: dbtest

import pymysql

# 连接 MySQL 数据库
conn = pymysql.connect(
    host='127.0.0.1',  # 主机名
    port=3306,         # 端口号，MySQL默认为3306
    user='roothjf',       # 用户名
    password='123456', # 密码
    database='zerorunner',   # 数据库名称
)

# 创建游标对象
cursor = conn.cursor()

# 执行 SQL 查询语句
cursor.execute("SELECT * FROM user")

# 获取查询结果
result = cursor.fetchall()
print(result)


